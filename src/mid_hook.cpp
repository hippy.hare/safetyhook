#include <algorithm>

#include <safetyhook/allocator.hpp>
#include <safetyhook/inline_hook.hpp>
#include <safetyhook/utility.hpp>

#include <safetyhook/mid_hook.hpp>

#include <zasm/zasm.hpp>

namespace safetyhook {

#ifdef _M_X64
constexpr uint8_t asm_data[] = {0x54, 0x55, 0x50, 0x53, 0x51, 0x52, 0x56, 0x57, 0x41, 0x50, 0x41, 0x51, 0x41, 0x52,
    0x41, 0x53, 0x41, 0x54, 0x41, 0x55, 0x41, 0x56, 0x41, 0x57, 0x9C, 0x48, 0x8D, 0x0C, 0x24, 0x48, 0x89, 0xE3, 0x48,
    0x83, 0xEC, 0x30, 0x48, 0x83, 0xE4, 0xF0, 0xFF, 0x15, 0x22, 0x00, 0x00, 0x00, 0x48, 0x89, 0xDC, 0x9D, 0x41, 0x5F,
    0x41, 0x5E, 0x41, 0x5D, 0x41, 0x5C, 0x41, 0x5B, 0x41, 0x5A, 0x41, 0x59, 0x41, 0x58, 0x5F, 0x5E, 0x5A, 0x59, 0x5B,
    0x58, 0x5D, 0x5C, 0xFF, 0x25, 0x08, 0x00, 0x00, 0x00, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90,
    0x90, 0x90, 0x90, 0x90, 0x90, 0x90};
#else
constexpr uint8_t asm_data[] = {0x54, 0x55, 0x50, 0x53, 0x51, 0x52, 0x56, 0x57, 0x9C, 0x54, 0xFF, 0x15, 0x00, 0x00,
    0x00, 0x00, 0x83, 0xC4, 0x04, 0x9D, 0x5F, 0x5E, 0x5A, 0x59, 0x5B, 0x58, 0x5D, 0x5C, 0xFF, 0x25, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
#endif

std::expected<MidHook, MidHook::Error> MidHook::create(void* target, MidHookFn destination) {
    return create(Allocator::global(), target, destination);
}

std::expected<MidHook, MidHook::Error> MidHook::create(
    const std::shared_ptr<Allocator>& allocator, void* target, MidHookFn destination) {
    MidHook hook{};

    if (const auto setup_result = hook.setup(allocator, reinterpret_cast<uint8_t*>(target), destination);
        !setup_result) {
        return std::unexpected{setup_result.error()};
    }

    return hook;
}

MidHook::MidHook(MidHook&& other) noexcept {
    *this = std::move(other);
}

MidHook& MidHook::operator=(MidHook&& other) noexcept {
    if (this != &other) {
        m_hook = std::move(other.m_hook);
        m_target = other.m_target;
        m_stub = std::move(other.m_stub);
        m_destination = other.m_destination;

        other.m_target = 0;
        other.m_destination = nullptr;
    }

    return *this;
}

void MidHook::reset() {
    *this = {};
}

std::expected<void, MidHook::Error> MidHook::setup(
    const std::shared_ptr<Allocator>& allocator, uint8_t* target, MidHookFn destination) {
    m_target = target;
    m_destination = destination;

    //////////////////////////
    using namespace zasm;

    Program program(MachineMode::AMD64);

    x86::Assembler a(program);

    // my code
    auto destination_ = a.createLabel();
    auto trampoline_ = a.createLabel();

    a.push(x86::rsp);
    a.push(x86::rbp);
    a.push(x86::rax);
    a.push(x86::rbx);
    a.push(x86::rcx);
    a.push(x86::rdx);
    a.push(x86::rsi);
    a.push(x86::rdi);
    a.push(x86::r8);
    a.push(x86::r9);
    a.push(x86::r10);
    a.push(x86::r11);
    a.push(x86::r12);
    a.push(x86::r13);
    a.push(x86::r14);
    a.push(x86::r15);
    a.pushfq();

    // ; set destination parameter (arg1)
    a.lea(x86::rcx, x86::qword_ptr(x86::rsp));

    // ; set destination parameter (arg2)
    a.mov(x86::rdx, (uint64_t)m_target);

    // ; align stack, save original
    a.mov(x86::rbx, x86::rsp);
    a.sub(x86::rsp, 48);
    a.and_(x86::rsp, -16);

    // ; call destination
    a.call(x86::qword_ptr(x86::rip, destination_));

    // ; restore stack
    a.mov(x86::rsp, x86::rbx);

    // ; restore context
    a.popfq();
    a.pop(x86::r15);
    a.pop(x86::r14);
    a.pop(x86::r13);
    a.pop(x86::r12);
    a.pop(x86::r11);
    a.pop(x86::r10);
    a.pop(x86::r9);
    a.pop(x86::r8);
    a.pop(x86::rdi);
    a.pop(x86::rsi);
    a.pop(x86::rdx);
    a.pop(x86::rcx);
    a.pop(x86::rbx);
    a.pop(x86::rax);
    a.pop(x86::rbp);
    a.pop(x86::rsp);

    // jmp [trampoline]
    a.jmp(x86::qword_ptr(x86::rip, trampoline_));

    a.bind(destination_);
    a.dq((uint64_t)m_destination);
    a.bind(trampoline_);
    a.dq(0x9090909090);

    Serializer serializer{};

    auto res = serializer.serialize(program, 0);

    // std::cout << getDisassemblyDump(serializer, program.getMode()) << "\n";

    //////////////////////////

    // int asm_size = sizeof(asm_data);
    size_t asm_size = serializer.getCodeSize();

    auto stub_allocation = allocator->allocate(asm_size);

    if (!stub_allocation) {
        return std::unexpected{Error::bad_allocation(stub_allocation.error())};
    }

    m_stub = std::move(*stub_allocation);

    std::copy_n(serializer.getCode(), asm_size, m_stub.data());

    /*
#ifdef _M_X64
    store(m_stub.data() + asm_size - 16, m_destination);
#else
    store(m_stub.data() + asm_size - 8, m_destination);

    // 32-bit has some relocations we need to fix up as well.
    store(m_stub.data() + 0xA + 2, m_stub.data() + asm_size - 8);
    store(m_stub.data() + 0x1C + 2, m_stub.data() + asm_size - 4);
#endif
    */

    auto hook_result = InlineHook::create(allocator, m_target, m_stub.data());

    if (!hook_result) {
        m_stub.free();
        return std::unexpected{Error::bad_inline_hook(hook_result.error())};
    }

    m_hook = std::move(*hook_result);

#ifdef _M_X64
    store(m_stub.data() + asm_size - 8, m_hook.trampoline().data());
#else
    store(m_stub.data() + asm_size - 4, m_hook.trampoline().data());
#endif

    return {};
}
} // namespace safetyhook
